package com.company;


import hangman.HangmanGame;
import hangmanGameSupport.HangmanSplashScreen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader input = new BufferedReader( new InputStreamReader(System.in));
        HangmanSplashScreen splashScreen = new HangmanSplashScreen();
        splashScreen.toDisplay();

        String inputStr;
        char inputChar;
        try
        {
            do {
                inputStr = input.readLine().toLowerCase();
                if (inputStr.length() != 0 && inputStr.charAt(0)!='q') {
                    inputChar = inputStr.charAt(0);
                    HangmanGame gameSession = new HangmanGame();
                    gameSession.play();
                    System.out.println("Wanna play again? Press (c)ontinue or (q)uit");
                } else{
                    break;
                }
            } while (inputChar != 'q');
        }catch(Error e){
            System.out.println("Nåt gick fel i main");
        }


        System.exit(0);


    }
}
