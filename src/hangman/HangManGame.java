package hangman;

import hangmanGameSupport.HangmanGuessWord;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Lasse on 2015-11-02.
 */
public class HangmanGame {

    public String playString;
    public HangmanPlayer player;
    public HangmanGuessWord word;

    public HangmanGame() {
        // HangmanSplashScreen splashScreen = new HangmanSplashScreen();
        //splashScreen.toDisplay();
        this.word = new HangmanGuessWord();
        this.playString = word.getRandomString();

        player = new HangmanPlayer(this.playString);
    }

    public void play() throws IOException {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        char inputChar;
        try
        {
            do {
                String inputStr = input.readLine().toLowerCase();
                if (inputStr.length() != 0 && inputStr.charAt(0)!='q') {
                    inputChar = inputStr.charAt(0);
                    this.player.updateResultOfGuess(inputChar);
                    this.player.toDisplay();
                } else {
                    break;
                }
            } while (inputChar != 'q' &&
                    (this.player.playerStatus != HangmanPlayer.PlayerState.lost &&
                            this.player.playerStatus != HangmanPlayer.PlayerState.win));

        } catch (Error e)
        {
            System.out.println("Det hände et fell i HangmanGme  " + e.toString());
        }
    }
}
