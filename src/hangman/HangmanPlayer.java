package hangman;

import java.util.ArrayList;

import static hangman.HangmanPlayer.PlayerState.*;

/**
 * Created by Lasse on 2015-11-02.
 */
public class HangmanPlayer {

    public String wordToBeDiscovered;
    public  String[] gameProgressDrawings;
    public ArrayList<Character> playersGuess;
    public ArrayList<Character> playersErrorGuess;
    public int startingDrawingLevel;
    public int currentDrawingLevel;
    public enum PlayerState {lost,win,playing};
    public PlayerState playerStatus;
    public int numberOfGuesses;
    public int loosingDrawingLevel;


   public HangmanPlayer(String guessingWord){
       this.numberOfGuesses = 0;
       this.playerStatus = playing;
       this.wordToBeDiscovered = guessingWord;
       this.playersGuess = new ArrayList<Character>();
       this.playersErrorGuess = new ArrayList<Character>();
       for (int i = 0; i < guessingWord.length();i++){
           playersGuess.add('*');
       }


       String manToBeHanged1 = "                    \n" +
                               "                    \n"  +
                               "                    \n"  +
                               "                    \n"  +
                               "                    \n"  +
                               "            |         "  +
                               " ";

       String manToBeHanged2 =  "                     \n" +
                                "                    \n"  +
                                "                    \n"  +
                                "                    \n"  +
                                "            |       \n"  +
                                "            |         "  +
                                 "";

       String manToBeHanged3 =  "                    \n" +
                                "                    \n"  +
                                "                    \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |         "  +
                                "";
       String manToBeHanged4 =  "                    \n" +
                                "                    \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |         "  +
                                "";
       String manToBeHanged5 =  "                    \n" +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |         "  +
                                 "";
       String manToBeHanged6 =  "            _       \n" +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |         "  +
                                "";

       String manToBeHanged7 =   "            __      \n" +
                                 "            |       \n"  +
                                 "            |       \n"  +
                                 "            |       \n"  +
                                 "            |       \n"  +
                                 "            |         "  +
                                 "";
       String manToBeHanged8 =  "            ___     \n" +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |         "  +
                                "";
       String manToBeHanged9 =  "            ____    \n" +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |         "  +
                                "";
       String manToBeHanged10=  "            _____   \n" +
                                "            |      \n"  +
                                "            |      \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |         "  +
                                "";
       String manToBeHanged11=  "            ______   \n" +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |         "  +
                                "";
       String manToBeHanged12=  "            ______   \n" +
                                "            |    |  \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |         "  +
                                "";
       String manToBeHanged13=  "            ______   \n" +
                                "            |    |  \n"  +
                                "            |    O  \n"  +
                                "            |       \n"  +
                                "            |       \n"  +
                                "            |         "  +
                                "";
       String manToBeHanged14=  "            ______   \n" +
                                "            |    |  \n"  +
                                "            |    O  \n"  +
                                "            |    |  \n"  +
                                "            |       \n"  +
                                "            |         "  +
                                "";
       String manToBeHanged15=  "            ______   \n" +
                                "            |    |  \n"  +
                                "            |    O  \n"  +
                                "            |   -|  \n"  +
                                "            |       \n"  +
                                "            |         "  +
                                "";
       String manToBeHanged16=  "            ______   \n" +
                                "            |    |  \n"  +
                                "            |    O  \n"  +
                                "            |   -|- \n"  +
                                "            |       \n"  +
                                "            |         "  +
                                "";
       String manToBeHanged17=  "            ______   \n" +
                                "            |    |  \n"  +
                                "            |    O  \n"  +
                                "            |   -|- \n"  +
                                "            |   /   \n"  +
                                "            |         "  +
                                "";
       String manToBeHanged18 =  "            ______   \n" +
                                "            |    |    \n"  +
                                "            |    O    \n"  +
                                "            |   -|-   \n"  +
                                "            |   / \\  \n"  +
                                "            |         \n"  +
                                "";

       gameProgressDrawings = new String[]{manToBeHanged1,
                                           manToBeHanged2,
                                           manToBeHanged3,
                                           manToBeHanged4,
                                           manToBeHanged5,
                                           manToBeHanged6,
                                           manToBeHanged7,
                                           manToBeHanged8,
                                           manToBeHanged9,
                                           manToBeHanged10,
                                           manToBeHanged11,
                                           manToBeHanged12,
                                           manToBeHanged13,
                                           manToBeHanged14,
                                           manToBeHanged15,
                                           manToBeHanged16,
                                           manToBeHanged17,
                                           manToBeHanged18};

       this.loosingDrawingLevel = this.wordToBeDiscovered.length();
       this.startingDrawingLevel =this.gameProgressDrawings.length - this.wordToBeDiscovered.length();
       this.currentDrawingLevel = this.startingDrawingLevel;

       //String tempArr1 = playersGuess.toString();
       System.out.println(playersGuess.toString());
        //System.out.println(tempArr1.toString());
        System.out.println(gameProgressDrawings[startingDrawingLevel]);
        //String tempArr2 = playersErrorGuess.toString();
        //System.out.println(tempArr2.toString());
   }
    public void toDisplay(){
        if (this.playerStatus == PlayerState.playing) {
            System.out.println(this.playersGuess.toString());
            System.out.println(gameProgressDrawings[currentDrawingLevel]);
            System.out.println(this.playersErrorGuess.toString());
        }else if(this.playerStatus == PlayerState.win){
            System.out.println(this.playersGuess.toString());
            System.out.println(gameProgressDrawings[currentDrawingLevel]);
            System.out.println("You won mother f-er!" );
        }else if(this.playerStatus == PlayerState.lost){
            System.out.println(playersGuess.toString());
            System.out.println(gameProgressDrawings[18-1]);
            System.out.println("!!!!!!!!!!HANGED!!!!!!!!\n" +
                    "press c to continue, q to quit");
        }
    }
    public void toDisplay(boolean winner){
        if(winner) {
            System.out.println(this.playersGuess.toArray().toString());
            System.out.println(gameProgressDrawings[currentDrawingLevel]);
            System.out.println("You won mother f-er!" );
        }else {
            System.out.println(this.playersGuess.toArray().toString());
            System.out.println(gameProgressDrawings[18-1]);
        }
    }

    public boolean isHanged(){
        if (this.currentDrawingLevel == 17)
            return true;
        else
            return false;
    }
    public boolean charInString(char c){
        boolean found = false;
        for (int i = 0; i < this.wordToBeDiscovered.length(); i++){
            if (c ==this.wordToBeDiscovered.charAt(i)){
                found = true;
                break;
            }
        }
        if(found)
            return true;
        else
            return false;
    }
    public void updateResultOfGuess (char c){
        this.numberOfGuesses++;  //increment the counter of the players guesses
        boolean found = false;
       // ArrayList<Integer> indexes = new ArrayList<Integer>();
        for (int i = 0; i < this.wordToBeDiscovered.length(); i++) {
            if (c == this.wordToBeDiscovered.charAt(i)) {
                //indexes.add(i);
                this.playersGuess.remove(i); //Remove  '*' from string of presentation
                this.playersGuess.add(i, c);  //Substitute the correct guessed character, everywhere
                found = true;
            }
        }
        if (!found){           //if guessed character not found
            this.playersErrorGuess.add(c);  //add this character to the list of erroroneous guesses
            this.currentDrawingLevel++; //increment the hanging drawing level
        }
        if (this.isWinner()){
            this.playerStatus = win;
        }
        else if (this.currentDrawingLevel == this.gameProgressDrawings.length -1)
            this.playerStatus = lost;


    }
    public boolean isWinner(){
        boolean theStringsAreTheSame = true;
           for( int i = 0; i < this.wordToBeDiscovered.length(); i++){
               theStringsAreTheSame = theStringsAreTheSame &&
                       (this.wordToBeDiscovered.charAt(i) == this.playersGuess.get(i));
           }
        return theStringsAreTheSame;

    }

}
