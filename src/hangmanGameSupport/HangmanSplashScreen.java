package hangmanGameSupport;

/**
 * Created by Lasse on 2015-11-02.
 */
public class HangmanSplashScreen {

    public  String[] splashScreen;

    public HangmanSplashScreen(){
        String base =          " /////////////////// ";
        String floor =         "____________________";
        String manToBeHanged = "            ______   \n" +
                               "            |    |  \n"  +
                               "            |    O  \n"  +
                               "        o   |       \n"  +
                               "       -|-  |       \n"  +
                               "       /\\   |         "  +
                               "";
        String welcomeText = "Welcome to HangmanPlayer\n"+
                             "guess words that are related to Java programming\n" +
                             " press c to continue, q to quit";

        splashScreen = new String[]{welcomeText, manToBeHanged, floor, base};
    }
    public void toDisplay(){
        for (int i = 0; i < this.splashScreen.length; i++){
            System.out.println(this.splashScreen[i]);

        };
    };
}
