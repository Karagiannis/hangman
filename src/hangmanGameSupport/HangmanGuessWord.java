package hangmanGameSupport;

/**
 * Created by Lasse on 2015-11-02.
 */
public class HangmanGuessWord {

    public int numberOfGuessStrings;
    public String[] strArray;

    public HangmanGuessWord(){

        String str0 = "hangman";
        String str1 = "extends";
        String str2 = "abstract";
        String str3 = "arraylist";
        String str4 = "containerclass";
        String str5 = "enum";
        String str6 = "finally";
        String str7 = "interface";

        this.strArray = new String[]{str0,str1,str2,str3,str4,str5,str6,str7};
        this.numberOfGuessStrings = strArray.length;
    }
    public String getRandomString(){
        int randomInt = (int) Math.floor(Math.random()*numberOfGuessStrings) +1;
        return this.strArray[randomInt];
    }

}
